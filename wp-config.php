<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4MbGOWS+THmvpTZU4WZUqzSncoJ4pndb+kQVP7YRY3uSyw/du4L0ObxxR3wpS+GB7aR7IXrhTgif3WkjFimzsA==');
define('SECURE_AUTH_KEY',  '70MrRWZY2VSIu5ME1Fg+d4NNh2NF35+pmTLykzJwzL7+Z2KQg/YhVucAAdqkyz0B7k0eDVqxVRvzdP8kaJBa8A==');
define('LOGGED_IN_KEY',    'z5vguAUceNeV6DsEBwlx+SJd9sKpMrBCad0a4R4oKtqq8JOAH2b+XnboVyI2FGyXV2VNJcYZumt2Rj4JCK03hQ==');
define('NONCE_KEY',        'x4PKc7PzJ3wNozSPmVhZEDb/GTDgMdRyz/F9Iq/AFTPYWBPtQQZaFlWXP65AJgbIptHhSh9qZc4pkkUjIokOYQ==');
define('AUTH_SALT',        'rRQqxabLeDx9/TuteZWdQSLKPHIGu+1UFjS50o0sTQrmwp16Lfda9G0uMbt4khiQdou5Wd9mcDqSgEvenA57cA==');
define('SECURE_AUTH_SALT', '5ISVp6M/LAHhcNwsU0iEyDPEFYbm0AQCsv+syTjCLd27ld9BcUEeE1aWbXY/PVPDwcv4h4BHHcHp40wnvOOmjQ==');
define('LOGGED_IN_SALT',   'doh5NQzZfGvX70vC6Qw/fFM1XFIvJsO6O3eeflbfz3PiQpgh/bHW1gpvRFiNjIIvB/1mNLGRxL4yf7S118zm7A==');
define('NONCE_SALT',       'zozD870QHyCt7FymEKD0SCK/qIzgx096Co55PIspFccCIvgGvgV0tmux33hTrULAdPRTEE8KFHZi+/SZomzIjg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
