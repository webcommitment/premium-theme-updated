<?php 
 
?>

<?php if ( have_rows( 'select_websites' ) ) : ?>
<section id="" class="home-services">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-12 col-lg-5 col-xl-5">
                <div class="row home-services__left">
                    <?php while ( have_rows( 'our_services' ) ) : the_row(); ?>
                    <div class="col-10">
                        <h5 class="home-services__sub-title animate-3"><?php echo get_sub_field( 'subtitle' ) ?></h5>
                        <h2 class="animate-4"><?php echo get_sub_field( 'main_title'); ?></h2>
                        <div class="home-services__content">
                   <?php echo get_sub_field( 'main_description'); ?>
                            <span>
                             <?php echo get_sub_field( 'secondary_description'); ?>
                            </span>
                        </div>
                        <div>
                            <a href="<?php echo get_sub_field( 'button_CTA' );?>" class="primary-btn">
                                <span><?php echo get_sub_field( 'button_title'); ?></span>
                            </a>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <div class="col-12 col-lg-7 col-xl-7 ">
                <div class="row home-services__right">
                    <?php while ( have_rows( 'select_websites' ) ) : the_row();
						$page = get_sub_field( 'page' );
                        $page_id = $page->ID;
                        $icon_page_main = get_field( 'icon', $page_id);
                    ?>
                    <div class="col-10 col-md-5 col-card home-services__item animate-2">
                        <div class="card">
                            <div class="card-thumbnail-wrapper">
                                <a href="<?php echo get_the_permalink($page->ID); ?>">
                                    <?php if(!empty ($icon_page_main)):?>
                                    <img src="<?php echo $icon_page_main['sizes']['thumbnail']; ?>" alt="icon">
                                    <?php endif;?>
                                </a>
                            </div>
                            <div class="card-content-wrapper">
                                <div class="card-title">
                                    <h5>
                                        <?php echo get_the_title($page->ID); ?>
                                    </h5>
                                </div>
                                <div class="card-content">
                                    <p>
                                        <?php echo get_the_excerpt($page->ID); ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card-cta">
                            <a href="<?php echo get_the_permalink($page->ID); ?>" class="secondary-btn">
                                <?php echo __('Lees meer', 'webcommitment-theme'); ?>
                            </a>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif;