<?php  
/**
 * Setup query to show the 'services' post type with '5' posts.
 * Output the title with content and author.
 */

    $args = array(  
        'post_type' => 'reviews',
        'post_status' => 'publish',
        'posts_per_page' => 5, 
        'orderby' => 'post_date', 
        'order' => 'ASC', 
    );
    $review = new WP_Query( $args ); 
?>

<section class="home-banner">
    <div id="home-banner-slider" class="home-banner__carousel carousel carousel-fade  slide" data-ride="carousel">
        <div class="home-banner__carousel-indicators">
            <ol class="carousel-indicators">
                <?php
            $k = 0;
            while ( $review->have_posts() ) : $review->the_post();  ?>
                <li data-target="#home-banner-slider" data-slide-to="<?php echo $k;?>"
                    class="<?php echo( $k == 0 ? 'active' : ''); ?>"></li>
                <?php 
            $k++;
            endwhile;
            wp_reset_postdata();
            ?>
            </ol>
        </div>
        <div class="carousel-inner">
            <?php
            $i = 0;
             while ( $review->have_posts() ) : $review->the_post();  ?>
            <div class="carousel-item <?php echo($i == 0 ? 'active' : ''); ?>">
                <div class="home-banner__carousel-content">
                    <div class="home-banner__carousel-inner">
                        <h3 class="animate"> <?php echo the_title(); ?> </h3>
                        <div class="animate"> <?php echo the_content();?> </div>
                        <h5 class="animate"> <?php echo $author_review = get_field( 'author_review'); ?></h5>
                    </div>
                </div>
            </div>
            <?php 
             $i++;
            endwhile;
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <div class="home-banner__carousel-nav">
        <a class="carousel-control-prev disable-anim" href="#home-banner-slider" role="button" data-slide="prev">
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next disable-anim" href="#home-banner-slider" role="button" data-slide="next">
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>