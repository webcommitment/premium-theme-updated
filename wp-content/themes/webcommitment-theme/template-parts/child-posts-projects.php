<?php
$args = array(
    'post_parent' => $post->ID,
    'post_type' => 'page',
    'orderby' => 'menu_order'
);

$child_query = new WP_Query( $args );

$id = the_post() -> ID;?>

<section id="" class="home-services">
  <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
        <div class="archive__wrapper">
          <?php while ( $child_query->have_posts() ) : $child_query->the_post(); ?>

          <div class="archive__item card">
            <div class="archive__img ">
              <?php  
        if ( has_post_thumbnail() ) {
            the_post_thumbnail('page-thumb-mine');
        }
        ?>
            </div>

            <div class="archive__content-wrapper">
              <div class="archive__content-wrapper-inner">
                <?php $iconPage = get_field( 'icon', $id); ?>
                <div class="archive__title">
                  <?php if(!empty ($iconPage)): ?>
                  <div class="archive__icon">
                    <img src="<?php echo $iconPage['url']; ?>" alt="" />
                  </div>
                  <?php endif; ?>
                  <h3><a href="<?php the_permalink(); ?>" rel="bookmark"
                      title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>

                </div>

                <div class="archive__description">
                  <?php the_excerpt(); ?>
                </div>
              </div>
              <div class="archive__btn">
                <a class="primary-btn" href="<?php the_permalink(); ?>" rel="bookmark"
                  title="<?php the_title(); ?>">Lees meer</a>
              </div>
            </div>
          </div>
          <?php endwhile; ?>
          <?php
wp_reset_postdata();?>

        </div>
      </div>
    </div>
  </div>
</section>