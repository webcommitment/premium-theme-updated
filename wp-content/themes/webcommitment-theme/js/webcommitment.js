(function ($) {
    // animations base
    function isInView(elem) {
        return $(elem).offset().top - $(window).scrollTop() < $(elem).height() + 450;
    }

    $(document).ready(function () {

        $('.main-menu__trigger').click(function () {
            $(this).toggleClass('active');
            $('header#masthead .lower-header').toggleClass('active');
            $('header#masthead .main-menu').toggleClass('active');
            $('body').toggleClass('active-menu');
        });

        $('.wpcf7 .wpcf7-submit').wrap('<div class="input-wrapper"></div>');

        $('.home-services__content p').addClass('animate-2');

        // animations
        $(window).on('scroll', function () {
            $(".animate").each(function () {
                if (isInView($(this))) {
                    $(this).addClass("in-viewport-fade-in");
                }
            });
        });

        $(window).on('scroll', function () {
            $(".animate-2").each(function () {
                if (isInView($(this))) {
                    $(this).addClass("in-viewport-full-opacity");
                }
            });
        });

        $(window).on('scroll', function () {
            $(".animate-3").each(function () {
                if (isInView($(this))) {
                    $(this).addClass("in-viewport-opacity");
                }
            });
        });

        $(window).on('scroll', function () {
            $(".animate-4").each(function () {
                if (isInView($(this))) {
                    $(this).addClass("in-viewport-rotate");
                }
            });
        });

    });
})(jQuery);