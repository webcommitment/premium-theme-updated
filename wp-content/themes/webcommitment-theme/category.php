<?php 

get_header();

$blog = get_queried_object();
$blog_id = $blog->cat_id;

?>
<article id="page-cat" class="blog-content">
    <section class="main-content">
        <header class="entry-header page__title">
            <div class="container-fluid">
                <?php echo single_cat_title( '<h1 class="entry-title">', '</h1>' ); ?>
                <p>
                    <?php echo category_description(); ?>
                </p>
                <div class="breadcrumbs">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </header><!-- .entry-header -->
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 col-md-12 col-xl-12">
                    <div class="col-11">
                        <p>
                            <?php echo category_description(); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php get_template_part( 'template-parts/content', 'newsarchive' ); ?>
    </section>
</article>
<?php
get_footer();