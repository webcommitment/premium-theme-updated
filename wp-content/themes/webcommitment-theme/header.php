<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webcommitment_Starter
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <!--    get analytics script-->
    <?php get_template_part('template-parts/analytics/content', 'head'); ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <!--    get analytics script-->
    <?php get_template_part('template-parts/analytics/content', 'body'); ?>
    <div id="page" class="site">
        <header id="masthead" class="site-header" role="banner">
            <div class="top-header">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col">

                            <div class="top-header__container">
                                <?php if (have_rows('usps', 'option')): ?>
                                <div class="header-usps">
                                    <?php while (have_rows('usps', 'option')): the_row();
                                        $icon = get_sub_field('usp_icon', 'option');
                                        $title = get_sub_field('usp_title', 'option');
                                        ?>
                                    <div class="header-usps__item">
                                        <div class="header-usps__icon">
                                            <img src="<?php echo $icon['url']; ?>" alt="<?php echo $title; ?>" />
                                        </div>
                                        <div class="header-usps__title">
                                            <?php echo $title; ?>
                                        </div>
                                    </div>
                                    <?php endwhile; ?>
                                </div>
                                <?php endif; ?>

                                <?php if (have_rows('contacts', 'option')): ?>
                                <div class="header-contacts">
                                    <?php while (have_rows('contacts', 'option')): the_row();
                                        $icon = get_sub_field('icon', 'option');
                                        $link = get_sub_field('link', 'option');
                                        ?>
                                    <div class="header-contacts__item">
                                        <div class="header-contacts__icon">
                                            <img src="<?php echo $icon['url']; ?>" alt="<?php echo $title; ?>" />
                                        </div>
                                        <a class="header-contacts__link" href="<?php echo $link['url']; ?>">
                                            <?php echo $link['title']; ?>
                                        </a>
                                    </div>
                                    <?php endwhile; ?>
                                </div>
                                <?php endif; ?>

                                <?php if (have_rows('socials', 'option')): ?>
                                <div class="header-socials">
                                    <?php while (have_rows('socials', 'option')): the_row();
                                        $icon = get_sub_field('socials_icon', 'option');
                                        $link = get_sub_field('socials_link', 'option');
                                        ?>
                                    <div class="header-socials__item">
                                        <a class="header-socials__link" href="<?php echo $link['url']; ?>"
                                            aria-label=" <?php echo $link['title']; ?>">
                                            <div class="header-socials__icon">
                                                <img src="<?php echo $link['url']; ?>"
                                                    alt="<?php echo $link['title']; ?>" />
                                            </div>
                                        </a>
                                    </div>
                                    <?php endwhile; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="lower-header">

                <div class="container-fluid">
                    <div class="row">
                        <div class="col">

                            <div class="lower-header__container">
                                <div class="logo-container">

                                    <?php the_custom_logo(); ?>
                                </div>
                                <nav class="main-menu">
                                    <div class="main-menu__container">
                                        <?php
                                    wp_nav_menu(array(
                                        'theme_location' => 'menu-1',
                                        'menu_id' => 'primary-menu',
                                    ));
                                    ?>

                                        <?php if (have_rows('main_ctas', 'option')): ?>
                                        <div class="contacts-block-small">

                                            <div class="contacts-block-small__title">
                                                <?php echo __('Contact us', 'webcommitment-theme'); ?>
                                            </div>

                                            <div class="contacts-block-small__items">
                                                <?php while (have_rows('main_ctas', 'option')): the_row();
                                                    $icon = get_sub_field('icon', 'option');
                                                    $link = get_sub_field('link', 'option');
                                                    ?>
                                                <div class="contacts-block-small__item">
                                                    <a class="contacts-block-small__link"
                                                        href="<?php echo $link['url']; ?>"
                                                        aria-label=" <?php echo $link['title']; ?>">
                                                        <div class="contacts-block-small__icon">
                                                            <img src="<?php echo $icon['url']; ?>"
                                                                alt="<?php echo $link['title']; ?>" />
                                                        </div>
                                                    </a>
                                                </div>
                                                <?php endwhile; ?>
                                            </div>
                                        </div>
                                        <?php endif; ?>
                                    </div>

                                    <div class="main-menu__trigger">
                                        <div></div>
                                    </div>

                                </nav>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </header><!-- #masthead -->
        <main>