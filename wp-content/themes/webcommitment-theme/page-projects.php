<?php
/**
* Template Name: Page with children
*
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package webcommitment_Starter
 */

get_header(); ?>
<article id="main-page">
    <section class="main-content">
        <div class="row justify-content-center">
            <div class="col-12 ">
                <?php
					    while ( have_posts() ) : the_post();

						    get_template_part( 'template-parts/content', 'contact' );

					    endwhile; // End of the loop.
					    ?>
            </div>
        </div>
    </section>
</article>
<?php get_template_part( 'template-parts/child-posts-projects' );?>

<?php
get_footer();