<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package webcommitment_Starter
 */

?>

</div><!-- #content -->
</main>
<footer>
    <div id="footer-wrapper">
        <div class="footer-top">
            <div class="container-fluid ">
                <div class="row justify-content-between">
                    <div class="col-12">
                        <?php get_template_part( 'template-parts/footer/content', 'prefooter' ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom ">
            <div class="container-fluid ">
                <div class="row justify-content-between">
                    <div class="col-12">
                        <?php get_template_part( 'template-parts/footer/content', 'footer' ); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>
</div>
<?php wp_footer(); ?>
</body>

</html>