<?php
/**
 * The template for displaying the front-page
 *
 * @package webcommitment_Starter
 */

get_header(); ?>
<article id="front-page">
    <section class="main-content">
        <?php
            while ( have_posts() ) : the_post();
                get_template_part( 'template-parts/blocks/content', 'header-slider' );
                get_template_part( 'template-parts/blocks/content', 'home-usps' );
                get_template_part( 'template-parts/blocks/content', 'home-portfolio-about' );
                get_template_part( 'template-parts/blocks/content', 'home-banner' );
                get_template_part( 'template-parts/blocks/content', 'home-services' );
                get_template_part( 'template-parts/content', 'newsletter' );
            endwhile; // End of the loop.
            ?>
    </section>
</article>
<?php
get_footer();